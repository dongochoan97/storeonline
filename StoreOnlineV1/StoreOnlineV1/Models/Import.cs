﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StoreOnlineV1.Models
{
    public class Import
    {
        public int Id { get; set; }

        public int IdProduct { get; set; }

        public int Quantity { get; set; }

        public decimal TotalMoney { get; set; }

        public DateTime? CreateDate { get; set; }

        public string CreateBy { get; set; }
    }
}
