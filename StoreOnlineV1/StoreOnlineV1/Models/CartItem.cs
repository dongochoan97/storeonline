﻿using System;
namespace StoreOnlineV1.Models
{
    public class CartItem
    {
        public int Quantity { set; get; }
        public long ProductId { get; set; }
    }
}
