﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreOnlineV1.Models
{
    public class Trademark
    {
        public int Id { get; set; }

        [StringLength(250)]
        [Display(Name = "Tên thương hiệu")]
        public string Name { get; set; }

        [Column(TypeName = "ntext")]
        [Display(Name = "Giới thiệu thương hiệu")]
        public string Description { get; set; }

        [StringLength(500)]
        [Display(Name = "Hình ảnh")]
        public string Image { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime? CreateDate { get; set; }

        [StringLength(50)]
        [Display(Name = "Tạo bởi")]
        public string CreateBy { get; set; }
    }
}
