﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreOnlineV1.Models
{
    public class Order
    {
        public int Id { get; set; }

        public int ID_User { get; set; }

        public DateTime? CreateDate { get; set; }

        [Column(TypeName = "money")]
        public decimal? Money { get; set; }

        public bool Status { get; set; }

        [Column(TypeName = "text")]
        public string Note { get; set; }
    }
}
