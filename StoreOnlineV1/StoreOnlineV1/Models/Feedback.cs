﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreOnlineV1.Models
{
    public class Feedback
    {
        public int Id { get; set; }

        [StringLength(50)]
        [Display(Name = "Tên Người phản hồi")]
        public string Name { get; set; }

        [StringLength(50)]
        [Display(Name = "Số điện thoại")]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [Column(TypeName = "ntext")]
        [Display(Name = "Nội dung")]
        public string Content { get; set; }

        [StringLength(250)]
        [Display(Name = "Địa chỉ")]
        public string Address { get; set; }

        [Display(Name = "Ngày phản hồi")]
        public DateTime? CreateDate { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? Status { get; set; }
    }
}
