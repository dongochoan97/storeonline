﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreOnlineV1.Models
{
    public class Product
    {
        public long Id { get; set; }

        [StringLength(250)]
        [Display(Name = "Tên sản phẩm")]
        public string Name { get; set; }

        [StringLength(20)]
        [Display(Name = "Mã sản phẩm")]
        public string Code { get; set; }

        [StringLength(500)]
        [Display(Name = "Mô tả ngắn")]
        public string Desciption { get; set; }

        [StringLength(500)]
        [Display(Name = "Ảnh lớn")]
        public string Image { get; set; }

        [Display(Name = "Giá nhập")]
        public decimal? Price { get; set; }

        [Display(Name = "Giảm giá")]
        public int? Discount { get; set; }

        [Display(Name = "Giá bán")]
        public decimal? PromotionPrice { get; set; }

        [Display(Name = "Thuế VAT")]
        public bool IncludedVAT { get; set; }

        [Display(Name = "Số luợng sản phảm")]
        public int? Quantity { get; set; }

        [Display(Name = "Thuộc danh mục")]
        public long CategoryID { get; set; }

        [Display(Name = "Thương hiệu")]
        public long TrademarkID { get; set; }

        [Column(TypeName = "ntext")]
        [Display(Name = "Chi tiết sản phẩm")]
        public string Detail { get; set; }

        [Display(Name = "Bảo hành")]
        public int? Warranty { get; set; }

        public DateTime? CreateDate { get; set; }

        [StringLength(50)]
        public string CreateBy { get; set; }

        [Display(Name = "Hiển thị")]
        public bool Status { get; set; }

        public int? ViewCount { get; set; }
    }
}
