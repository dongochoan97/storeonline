﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StoreOnlineV1.Models
{
    public class Introduce
    {
        public int Id { get; set; }

        [Display(Name = "Nội dung")]
        public string Content { get; set; }
        [Display(Name = "Liên hệ")]
        public string Phone { get; set; }
        [Display(Name = "Địa chỉ")]
        public string Address { get; set; }

        public string Email { get; set; }
        [Display(Name = "Ghi chú")]
        public string Note { get; set; }

    }
}
