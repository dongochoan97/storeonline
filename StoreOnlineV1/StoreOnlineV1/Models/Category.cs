﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StoreOnlineV1.Models
{
    public class Category
    {
        public int Id { get; set; }
        
        [StringLength(250)]
        [Display(Name = "Tên danh mục")]
        public string Name { get; set; }

        [StringLength(250)]
        [Display(Name = "Tiêu đề")]
        public string MetaTitle { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime? CreateDate { get; set; }

        [StringLength(50)]
        [Display(Name = "Tạo bởi")]
        public string CreateBy { get; set; }

        [Display(Name = "Ngày cập nhật")]
        public DateTime? ModifiedDate { get; set; }

        [StringLength(50)]
        [Display(Name = "Cập nhật bởi")]
        public string ModifiedBy { get; set; }

        [Display(Name = "Hiển thị trang chủ")]
        public bool ShowOnHome { get; set; }
    }
}
