﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StoreOnlineV1.Models
{
    public class OrderDetail
    {
        public int Id { get; set; }

        public long ID_Order { get; set; }

        public long ID_Product { get; set; }

        public int Quantity { get; set; }

        public decimal? PriceImport { get; set; }

        public decimal? Price { get; set; }

        public int? Sale { get; set; }

    }
}
