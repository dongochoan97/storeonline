﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StoreOnlineV1.Models
{
    public class User
    {
        public int Id { get; set; }

        [StringLength(50)]
        [Required]
        [Display(Name = "Tên tài khoản")]
        public string UserName { get; set; }

        [StringLength(32)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [StringLength(50)]
        [Required]
        [Display(Name = "Họ tên")]
        public string Name { get; set; }

        [StringLength(250)]
        [Required]
        [Display(Name = "Địa chỉ")]
        public string Address { get; set; }

        [StringLength(50), EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [StringLength(10)]
        [Display(Name = "Số điện thoại")]
        public string Phone { get; set; }

        public DateTime? CreateDate { get; set; }

        [StringLength(50)]
        public string CreateBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        [StringLength(50)]
        public string ModifiedBy { get; set; }

        [Display(Name = "Trạng thái")]
        public bool Status { get; set; }
    }
}
