﻿var user = {
    innt: function () {
        user.registerEvent();
    },
    registerEvent: function () {
        $('btnactive').off('click').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var id = btn.data('id');
            $.ajax({
                url: "/Admin/User/ChangeStatus",
                data: { id: id },
                dataType: "json",
                type:"POST",
                contenttype: "application/json;charset=utf-8",
                success: function (response) {
                    if (response.status == true) {
                        $(this).text('Kích hoạt');
                    } else {
                        $(this).text('Khóa');
                    }
                }
            });
        });
    }
}
user.innt();