﻿var product = {
    init: function () {
        product.registerEvent();
    },
    registerEvent: function () {
                    

        $('#btnSelectImage').on('click', function (e) {
            e.preventDefault();

            var finder = new CKFinder();
            finder.selectActionFunction = function (fileUrl) {
                $('#textimage').val(fileUrl);
                $('#imganhlon').attr('src', fileUrl);
                console.log(fileUrl);
            };
            finder.popup();
        });

        /////////////
        $('#btnchonanhcon').off('click').on('click', function (e) {
            e.preventDefault();
            $('#imageManager').modal('show');
            

        });

        /// chọn ảnh ở popup
        $('#btnChoImage').off('click').on('click', function (e) {
            e.preventDefault();

            var finder = new CKFinder();
            finder.selectActionFunction = function (fileUrl) {
                $('#imageList').append('<div style="float:left"><img src="' + fileUrl + '" width="100"/><a href="#" class="btn-delImages"><i class="fa fa-times"></i></a></div>');

                $('.btn-delImages').off('click').on('click', function (e) { // xoas image ra khoi list
                    e.preventDefault();
                    $(this).parent().remove();

                });
            };
            finder.popup();


        });
        /// btnSaveImage
        $('#btnSaveImage').off('click').on('click', function () {
                var image =[];
               
                $.each($('#imageList img'), function (i, item) {
                    image.push($(item).prop('src'));
                    
                    $('#textimagecon').val(JSON.stringify(image));
                    $('#imageManager').modal('hide');
            })
                 
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview_image').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#textimage").change(function () {
            readURL(this);
        });
}
product.init();