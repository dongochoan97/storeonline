using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using StoreOnlineV1.Data;
using StoreOnlineV1.Models;

namespace StoreOnlineV1.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class IntroducesController : Controller
    {
        private readonly StoreContext _context;

        public IntroducesController(StoreContext context)
        {
            _context = context;
        }

        // GET: Introduces
        [Obsolete]
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session;
            string jsonUser = session.GetString("userinfo");
            if (jsonUser != null)
            {
                var userinfo = JsonConvert.DeserializeObject<User>(jsonUser);
                if (await CheckLogin(userinfo) == false)
                {
                    return RedirectToAction("Login", "Acount", new { Area = "" });
                }
                ViewData["UserName"] = userinfo.UserName;
            }
            else
            {
                return RedirectToAction("Login", "Acount", new { Area = "" });
            }
            return View(await _context.Introduce.ToListAsync());
        }
        [Obsolete]
        public async Task<bool> CheckLogin(User user)
        {
            var _user = await _context.User.FirstOrDefaultAsync(m => m.Id == user.Id);
            if (user.Password == _user.Password && _user.Status == true) return true;
            return false;
        }
        // GET: Introduces/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var introduce = await _context.Introduce
                .FirstOrDefaultAsync(m => m.Id == id);
            if (introduce == null)
            {
                return NotFound();
            }

            return View(introduce);
        }

        // GET: Introduces/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Introduces/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Content,Phone,Address,Email,Note")] Introduce introduce)
        {
            if (ModelState.IsValid)
            {
                _context.Add(introduce);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(introduce);
        }

        // GET: Introduces/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var introduce = await _context.Introduce.FindAsync(id);
            if (introduce == null)
            {
                return NotFound();
            }
            return View(introduce);
        }

        // POST: Introduces/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Content,Phone,Address,Email,Note")] Introduce introduce)
        {
            if (id != introduce.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(introduce);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!IntroduceExists(introduce.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(introduce);
        }

        // GET: Introduces/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var introduce = await _context.Introduce
                .FirstOrDefaultAsync(m => m.Id == id);
            if (introduce == null)
            {
                return NotFound();
            }

            return View(introduce);
        }

        // POST: Introduces/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var introduce = await _context.Introduce.FindAsync(id);
            _context.Introduce.Remove(introduce);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool IntroduceExists(int id)
        {
            return _context.Introduce.Any(e => e.Id == id);
        }
    }
}
