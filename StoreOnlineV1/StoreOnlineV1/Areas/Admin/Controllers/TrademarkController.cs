using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using StoreOnlineV1.Data;
using StoreOnlineV1.Models;

namespace StoreOnlineV1.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class TrademarkController : Controller
    {
        private readonly StoreContext _context;
        public const string USERKEY = "userinfo";
        [Obsolete]
        private readonly IHostingEnvironment _appEnvironment;


        [Obsolete]
        public TrademarkController(StoreContext context, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }

        // GET: Trademark
        [Obsolete]
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session;
            string jsonUser = session.GetString(USERKEY);
            if (jsonUser != null)
            {
                var userinfo = JsonConvert.DeserializeObject<User>(jsonUser);
                if (await CheckLogin(userinfo) == false)
                {
                    return RedirectToAction("Login", "Acount", new { Area = "" });
                }
                ViewData["UserName"] = userinfo.UserName;
            }
            else
            {
                return RedirectToAction("Login", "Acount", new { Area = "" });
            }
            return View(await _context.Trademark.ToListAsync());
        }
        [Obsolete]
        public async Task<bool> CheckLogin(User user)
        {
            var _user = await _context.User.FirstOrDefaultAsync(m => m.Id == user.Id);
            if (user.Password == _user.Password && _user.Status == true) return true;
            return false;
        }

        // GET: Trademark/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var import = await _context.Trademark
                .FirstOrDefaultAsync(m => m.Id == id);
            if (import == null)
            {
                return NotFound();
            }

            return View(import);
        }

        // GET: Trademark/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Trademark/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Obsolete]
        public async Task<IActionResult> Create( Trademark trademark)
        {
            if (ModelState.IsValid)
            {
                var files = HttpContext.Request.Form.Files;
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {
                        var file = Image;
                        //There is an error here
                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "uploads");
                        if (file.Length > 0)
                        {
                            var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                trademark.Image = "/uploads/" + fileName;
                            }

                        }
                    }
                }
                _context.Add(trademark);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(trademark);
        }

        // GET: Trademark/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var import = await _context.Trademark.FindAsync(id);
            if (import == null)
            {
                return NotFound();
            }
            return View(import);
        }

        // POST: Trademark/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Trademark trademark)
        {
            if (id != trademark.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(trademark);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ImportExists(trademark.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(trademark);
        }

        // GET: Trademark/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trademark = await _context.Trademark
                .FirstOrDefaultAsync(m => m.Id == id);
            if (trademark == null)
            {
                return NotFound();
            }

            return View(trademark);
        }

        // POST: Trademark/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var import = await _context.Trademark.FindAsync(id);
            _context.Trademark.Remove(import);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ImportExists(int id)
        {
            return _context.Trademark.Any(e => e.Id == id);
        }
    }
}
