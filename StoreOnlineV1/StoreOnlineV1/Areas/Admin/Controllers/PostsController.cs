using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using StoreOnlineV1.Data;
using StoreOnlineV1.Models;

namespace StoreOnlineV1.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class PostsController : Controller
    {
        private readonly StoreContext _context;
        [Obsolete]
        private readonly IHostingEnvironment _appEnvironment;

        [Obsolete]
        public PostsController(StoreContext context, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }

        // GET: Posts
        [Obsolete]
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session;
            string jsonUser = session.GetString("userinfo");
            if (jsonUser != null)
            {
                var userinfo = JsonConvert.DeserializeObject<User>(jsonUser);
                if (await CheckLogin(userinfo) == false)
                {
                    return RedirectToAction("Login", "Acount", new { Area = "" });
                }
                ViewData["UserName"] = userinfo.UserName;
            }
            else
            {
                return RedirectToAction("Login", "Acount", new { Area = "" });
            }
            var allPosts = await _context.Posts.ToListAsync();

            return View(allPosts);
        }
        [Obsolete]
        public async Task<bool> CheckLogin(User user)
        {
            var _user = await _context.User.FirstOrDefaultAsync(m => m.Id == user.Id);
            if (user.Password == _user.Password && _user.Status == true) return true;
            return false;
        }

        // GET: Posts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Posts = await _context.Posts
                .FirstOrDefaultAsync(m => m.Id == id);
            if (Posts == null)
            {
                return NotFound();
            }

            return View(Posts);
        }

        // GET: Posts/Create
        public IActionResult Create()
        {
            
            return View();
        }

        // POST: Posts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Obsolete]
        public async Task<IActionResult> Create(Posts posts)
        {
            if (ModelState.IsValid)
            {
                var files = HttpContext.Request.Form.Files;
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {
                        var file = Image;
                        //There is an error here
                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "uploads");
                        if (file.Length > 0)
                        {
                            var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                posts.Image = "/uploads/"+fileName;
                            }

                        }
                    }
                }
                posts.CreateDate = DateTime.Now;
                posts.CreateBy = "Admin";
                _context.Add(posts);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(posts);
        }

        // GET: Posts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Posts = await _context.Posts.FindAsync(id);
            if (Posts == null)
            {
                return NotFound();
            }
            return View(Posts);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Obsolete]
        public async Task<IActionResult> Edit(int id, Posts posts)
        {
            if (id != posts.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var files = HttpContext.Request.Form.Files;
                    foreach (var Image in files)
                    {
                        if (Image != null && Image.Length > 0)
                        {
                            var file = Image;
                            //There is an error here
                            var uploads = Path.Combine(_appEnvironment.WebRootPath, "uploads");
                            if (file.Length > 0)
                            {
                                var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                                using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                                {
                                    await file.CopyToAsync(fileStream);
                                    posts.Image = "/uploads/" + fileName;
                                }

                            }
                        }
                    }
                    _context.Update(posts);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PostsExists(posts.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(posts);
        }

        // GET: Posts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Posts = await _context.Posts
                .FirstOrDefaultAsync(m => m.Id == id);

            if (Posts == null)
            {
                return NotFound();
            }

            return View(Posts);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var Posts = await _context.Posts.FindAsync(id);
            _context.Posts.Remove(Posts);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PostsExists(long id)
        {
            return _context.Posts.Any(e => e.Id == id);
        }
    }
}
