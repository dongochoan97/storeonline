using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using StoreOnlineV1.Data;
using StoreOnlineV1.Models;

namespace StoreOnlineV1.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ImportsController : Controller
    {
        private readonly StoreContext _context;

        public ImportsController(StoreContext context)
        {
            _context = context;
        }

        // GET: Importss
        [Obsolete]
        public async Task<IActionResult> Index()
        {
            var session = HttpContext.Session;
            string jsonUser = session.GetString("userinfo");
            if (jsonUser != null)
            {
                var userinfo = JsonConvert.DeserializeObject<User>(jsonUser);
                if (await CheckLogin(userinfo) == false)
                {
                    return RedirectToAction("Login", "Acount", new { Area = "" });
                }
                ViewData["UserName"] = userinfo.UserName;
            }
            else
            {
                return RedirectToAction("Login", "Acount", new { Area = "" });
            }
            return View(await _context.Import.ToListAsync());
        }

        // GET: Importss/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Imports = await _context.Import
                .FirstOrDefaultAsync(m => m.Id == id);
            if (Imports == null)
            {
                return NotFound();
            }

            return View(Imports);
        }
        [Obsolete]
        public async Task<bool> CheckLogin(User user)
        {
            var _user = await _context.User.FirstOrDefaultAsync(m => m.Id == user.Id);
            if (user.Password == _user.Password && _user.Status == true) return true;
            return false;
        }

        // GET: Importss/Create
        public IActionResult Create()
        {
            var listProduct = _context.Product.ToList();
            var data = new List<SelectListItem> { };
            foreach (var item in listProduct)
            {
                data.Add(new SelectListItem { Value = item.Id.ToString(), Text = item.Name });
            }
            ViewBag.ListProduct = data;
            return View();
        }

        // POST: Importss/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create( Import imports)
        {
            if (ModelState.IsValid)
            {
                imports.CreateDate = DateTime.Now;
                imports.CreateBy = "Admin";
                _context.Add(imports);
                var product = await _context.Product.FirstOrDefaultAsync(m => m.Id == imports.IdProduct);
                var num = product.Quantity + imports.Quantity;
                var price = (decimal)imports.TotalMoney / imports.Quantity;
                product.Quantity = num;
                product.Price = price;
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(imports);
        }

        // GET: Importss/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var listProduct = _context.Product.ToList();
            var data = new List<SelectListItem> { };
            foreach (var item in listProduct)
            {
                data.Add(new SelectListItem { Value = item.Id.ToString(), Text = item.Name });
            }
            ViewBag.ListProduct = data;
            var Imports = await _context.Import.FindAsync(id);
            if (Imports == null)
            {
                return NotFound();
            }
            return View(Imports);
        }

        // POST: Importss/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Import imports)
        {
            if (id != imports.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var product = await _context.Product.FirstOrDefaultAsync(m => m.Id == imports.IdProduct);
                    var num = product.Quantity + imports.Quantity;
                    var price = (decimal)imports.TotalMoney / imports.Quantity;
                    product.Quantity = num;
                    product.Price = price;
                    _context.Update(imports);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ImportsExists(imports.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(imports);
        }

        // GET: Importss/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Imports = await _context.Import
                .FirstOrDefaultAsync(m => m.Id == id);
            if (Imports == null)
            {
                return NotFound();
            }

            return View(Imports);
        }

        // POST: Importss/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var Imports = await _context.Import.FindAsync(id);
            _context.Import.Remove(Imports);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ImportsExists(int id)
        {
            return _context.Import.Any(e => e.Id == id);
        }
    }
}
