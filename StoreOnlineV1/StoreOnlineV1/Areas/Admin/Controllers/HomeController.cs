﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using StoreOnlineV1.Data;
using StoreOnlineV1.Models;

namespace StoreOnlineV1.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class HomeController : Controller
    {

        private readonly StoreContext _context;
        [Obsolete]
        private readonly IHostingEnvironment _appEnvironment;
        public const string USERKEY = "userinfo";
        [Obsolete]
        public HomeController(StoreContext context, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }


        [Obsolete]
        public async Task<IActionResult> Index(ReportViewModel data)
        {

            //
            var session = HttpContext.Session;
            string jsonUser = session.GetString(USERKEY);
            if (jsonUser != null)
            {
                var userinfo = JsonConvert.DeserializeObject<User>(jsonUser);
                decimal totalImportPrice = 0;
                if (await CheckLogin(userinfo))
                {
                    ViewData["UserName"] = userinfo.UserName;
                    var allDetailProducts = await _context.OrderDetail.ToListAsync();

                    var lstModel = new List<ReportTopProductViewModel>();

                    if (data.StartTime != null)
                    {
                        for (var i = 0; i < allDetailProducts.Count; i++)
                        {
                            var order = await _context.Order.FirstOrDefaultAsync(m => m.Id == allDetailProducts[i].ID_Order);
                            if (order.CreateDate >= data.StartTime && order.CreateDate <= data.EndTime)
                            {
                                var cartitem = lstModel.Find(p => p.Id == allDetailProducts[i].ID_Product);
                                if (cartitem != null)
                                {
                                    // Đã tồn tại, tăng thêm 1
                                    cartitem.Quantity += allDetailProducts[i].Quantity;
                                    totalImportPrice += (decimal)(allDetailProducts[i].PriceImport * allDetailProducts[i].Quantity);
                                }
                                else
                                {
                                    totalImportPrice += (decimal)(allDetailProducts[i].PriceImport * allDetailProducts[i].Quantity);
                                    //  Thêm mới
                                    var product = await _context.Product.FirstOrDefaultAsync(m => m.Id == allDetailProducts[i].ID_Product);
                                    lstModel.Add(new ReportTopProductViewModel
                                    {
                                        Id = (int)allDetailProducts[i].ID_Product,
                                        Name = product.Name,
                                        Quantity = allDetailProducts[i].Quantity
                                    });

                                }
                            }
                        }
                    }
                    else
                    {
                        for (var i = 0; i < allDetailProducts.Count; i++)
                        {
                            var cartitem = lstModel.Find(p => p.Id == allDetailProducts[i].ID_Product);
                            if (cartitem != null)
                            {
                                // Đã tồn tại, tăng thêm 1
                                cartitem.Quantity += allDetailProducts[i].Quantity;
                                totalImportPrice += (decimal)(allDetailProducts[i].PriceImport * allDetailProducts[i].Quantity);
                            }
                            else
                            {
                                //  Thêm mới
                                var product = await _context.Product.FirstOrDefaultAsync(m => m.Id == allDetailProducts[i].ID_Product);
                                totalImportPrice += (decimal)(allDetailProducts[i].PriceImport * allDetailProducts[i].Quantity);
                                lstModel.Add(new ReportTopProductViewModel
                                {
                                    Id = (int)allDetailProducts[i].ID_Product,
                                    Name = product.Name,
                                    Quantity = allDetailProducts[i].Quantity
                                });

                            }
                        }
                    }
                    lstModel = lstModel.OrderByDescending(s => s.Quantity).ToList();
                    var lstModelNew = new List<ReportTopProductViewModel>();
                    if (lstModel.Count > 10)
                    {
                        for (var j = 0; j < 10; j++)
                        {
                            lstModelNew.Add(lstModel[j]);
                        }
                    }
                    else
                    {
                        lstModelNew = lstModel;
                    }
                    lstModelNew = lstModelNew.OrderBy(s => s.Id).ToList();
                    // thong ke doanh thu
                    var allOrder = new List<Order>();
                    if (data.StartTime == null)
                    {
                        allOrder = await _context.Order.ToListAsync();
                    }
                    else
                    {
                        allOrder = await _context.Order.Where(X => X.CreateDate >= data.StartTime && X.CreateDate <= data.EndTime).ToListAsync();
                    }

                    decimal total = 0;
                    allOrder.ForEach(item =>
                    {
                        total += (decimal)item.Money;
                    });
                    
                    ViewData["Total"] = total;
                    ViewData["TotalOrder"] = allOrder.Count;
                    ViewData["TotalImport"] = totalImportPrice;
                    ViewData["TotalRevenue"] = total - totalImportPrice;
                    ViewBag.listModelNew = lstModelNew;

                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Acount", new { Area = "" });
                }
            }
            else
            {
                return RedirectToAction("Login", "Acount", new { Area = "" });
            }
        }


        [Obsolete]
        public async Task<bool> CheckLogin(User user)
        {
            var _user = await _context.User.FirstOrDefaultAsync(m => m.Id == user.Id);
            if (user.Password == _user.Password && _user.Status == true) return true;
            return false;
        }

    }
}
