using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using StoreOnlineV1.Data;
using StoreOnlineV1.Models;

namespace StoreOnlineV1.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ProductsController : Controller
    {
        private readonly StoreContext _context;
        public const string USERKEY = "userinfo";
        [Obsolete]
        private readonly IHostingEnvironment _appEnvironment;

        [Obsolete]
        public ProductsController(StoreContext context, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }

        // GET: Products
        [Obsolete]
        public async Task<IActionResult> Index(string searchString)
        {
            var session = HttpContext.Session;
            string jsonUser = session.GetString(USERKEY);
            if (jsonUser != null)
            {
                var userinfo = JsonConvert.DeserializeObject<User>(jsonUser);
                if (await CheckLogin(userinfo) == false)
                {
                    return RedirectToAction("Login", "Acount", new { Area = "" });
                }
                ViewData["UserName"] = userinfo.UserName;
            }
            else
            {
                return RedirectToAction("Login", "Acount", new { Area = "" });
            }
            var allProducts = new List<Product>();
            if (!String.IsNullOrEmpty(searchString))
            {
                allProducts = await _context.Product.Where(s => s.Name.Contains(searchString)).ToListAsync();
            }
            else
            {
                allProducts = await _context.Product.ToListAsync();
            }
            var listProductsView = new List<ProductViewModel> { };
            foreach (var item in allProducts)
            {
                var product = new ProductViewModel();
                product.Id = item.Id;
                product.Name = item.Name;
                product.Code = item.Code;
                if (item.Image != "" && item.Image != null)
                {
                    product.Image = item.Image;
                }
                product.Price = item.Price;
                product.PromotionPrice = item.PromotionPrice;
                product.IncludedVAT = item.IncludedVAT;
                product.CategoryID = item.CategoryID;
                product.Warranty = item.Warranty;
                product.Status = item.Status;
                product.ViewCount = item.ViewCount;
                product.Quantity = item.Quantity;
                var categorys = await _context.Categories.FirstOrDefaultAsync(m => m.Id == item.CategoryID);
                product.CategoryName = categorys.Name;
                listProductsView.Add(product);
            }

            return View(listProductsView);
        }
        // GET: Products/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Product
                .FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // GET: Products/Create
        public IActionResult Create()
        {
            var listProductCategory = _context.ProductCategory.ToList();
            var data = new List<SelectListItem> { };
            foreach (var item in listProductCategory)
            {
                data.Add(new SelectListItem { Value = item.Id.ToString(), Text = item.Name });
            }
            ViewBag.ListCategories = data;
            var listThuongHieu = _context.Trademark.ToList();
            var dsThuongHieu = new List<SelectListItem> { };
            foreach (var item in listThuongHieu)
            {
                dsThuongHieu.Add(new SelectListItem { Value = item.Id.ToString(), Text = item.Name });
            }
            ViewBag.ListTrademark = dsThuongHieu;
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Obsolete]
        public async Task<IActionResult> Create(Product product)
        {
            if (ModelState.IsValid)
            {
                var files = HttpContext.Request.Form.Files;
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {
                        var file = Image;
                        //There is an error here
                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "uploads");
                        if (file.Length > 0)
                        {
                            var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                product.Image = "/uploads/"+fileName;
                            }

                        }
                    }
                }
                product.CreateDate = DateTime.Now;
                product.CreateBy = "Admin";
                product.Quantity = 0;
                product.ViewCount = 0;
                _context.Add(product);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Product.FindAsync(id);
            var listProductCategory = _context.ProductCategory.ToList();
            var data = new List<SelectListItem> { };
            foreach (var item in listProductCategory)
            {
                data.Add(new SelectListItem { Value = item.Id.ToString(), Text = item.Name });
            }
            ViewBag.ListCategories = data;
            var listThuongHieu = _context.Trademark.ToList();
            var dsThuongHieu = new List<SelectListItem> { };
            foreach (var item in listThuongHieu)
            {
                dsThuongHieu.Add(new SelectListItem { Value = item.Id.ToString(), Text = item.Name });
            }
            ViewBag.ListTrademark = dsThuongHieu;
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Obsolete]
        public async Task<IActionResult> Edit(long id, Product product)
        {
            if (id != product.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var files = HttpContext.Request.Form.Files;
                    foreach (var Image in files)
                    {
                        if (Image != null && Image.Length > 0)
                        {
                            var file = Image;
                            //There is an error here
                            var uploads = Path.Combine(_appEnvironment.WebRootPath, "uploads");
                            if (file.Length > 0)
                            {
                                var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                                using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                                {
                                    await file.CopyToAsync(fileStream);
                                    product.Image = "/uploads/" + fileName;
                                }

                            }
                        }
                    }
                    //
                    _context.Update(product);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Product
                .FirstOrDefaultAsync(m => m.Id == id);

            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var product = await _context.Product.FindAsync(id);
            _context.Product.Remove(product);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductExists(long id)
        {
            return _context.Product.Any(e => e.Id == id);
        }
        [Obsolete]
        public async Task<bool> CheckLogin(User user)
        {
            var _user = await _context.User.FirstOrDefaultAsync(m => m.Id == user.Id);
            if (user.Password == _user.Password && _user.Status == true) return true;
            return false;
        }
    }
}
