﻿using System;
namespace StoreOnlineV1.Models
{
    public class ReportTopProductViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}
