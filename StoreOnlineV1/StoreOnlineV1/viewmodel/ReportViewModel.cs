﻿using System;

namespace StoreOnlineV1.Models
{
    public class ReportViewModel
    {
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
    }
}
