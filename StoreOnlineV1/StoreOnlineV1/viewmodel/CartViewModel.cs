﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreOnlineV1.Models
{
    public class CartViewModel
    {
        public long Id { get; set; }

        [StringLength(250)]
        [Display(Name = "Tên sản phẩm")]
        public string Name { get; set; }

        [StringLength(20)]
        [Display(Name = "Mã sản phẩm")]
        public string Code { get; set; }

        [StringLength(250)]
        [Display(Name = "Ảnh lớn")]
        public string Image { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }
        public string Address { get; set; }

    }
}
