﻿
using System.ComponentModel.DataAnnotations;

namespace StoreOnlineV1.Models
{
    public class PayViewModel
    { 

        [StringLength(250)]
        public string Phone { get; set; }

        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Note { get; set; }

        [StringLength(500)]
        public string Address { get; set; }
    }
}
