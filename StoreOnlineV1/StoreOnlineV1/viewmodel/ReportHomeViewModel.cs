﻿using System;
using System.Collections.Generic;
using StoreOnlineV1.Models;

namespace StoreOnlineV1.Models
{
    public class ReportHomeViewModel
    {
        public List<ReportTopProductViewModel> reportTopProductViewModel { get; set; }
        public ReportViewModel reportViewModel { get; set; }

    }
}
