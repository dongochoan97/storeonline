﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreOnlineV1.Models
{
    public class ProductViewModel
    {
        public long Id { get; set; }

        [StringLength(250)]
        [Display(Name = "Tên sản phẩm")]
        public string Name { get; set; }

        [StringLength(20)]
        [Display(Name = "Mã sản phẩm")]
        public string Code { get; set; }

        [StringLength(250)]
        [Display(Name = "Ảnh lớn")]
        public string Image { get; set; }

        [Display(Name = "Giá nhập")]
        public decimal? Price { get; set; }

        [Display(Name = "Giá bán")]
        public decimal? PromotionPrice { get; set; }

        [Display(Name = "Thuế VAT")]
        public bool? IncludedVAT { get; set; }

        [Display(Name = "Thuộc danh mục")]
        public long? CategoryID { get; set; }

        public string CategoryName { get; set; }

        [Display(Name = "Bảo hành")]
        public int? Warranty { get; set; }

        [Display(Name = "Hiển thị")]
        public bool Status { get; set; }

        public int? ViewCount { get; set; }
        public int? Quantity { get; set; }
    }
}
