﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StoreOnlineV1.Models
{
    public class DetailPayModel
    {
        public long Id { get; set; }

        public long ID_Order { get; set; }
        [StringLength(250)]
        [Display(Name = "Tên sản phẩm")]
        public string Name { get; set; }

        [StringLength(20)]
        [Display(Name = "Mã sản phẩm")]
        public string Code { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public int Sale { get; set; }

        public decimal Total { get; set; }

    }
}
