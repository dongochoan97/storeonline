﻿using System;
using Microsoft.EntityFrameworkCore;
using StoreOnlineV1.Models;

namespace StoreOnlineV1.Data
{
    public class StoreContext : DbContext
    {
        public StoreContext(DbContextOptions<StoreContext> options) : base(options)
        {

        }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Feedback> Feedback { get; set; }

        public DbSet<Order> Order { get; set; }

        public DbSet<OrderDetail> OrderDetail { get; set; }

        public DbSet<Product> Product { get; set; }

        public DbSet<User> User { get; set; }

        public DbSet<Introduce> Introduce { get; set; }

        public DbSet<Import> Import { get; set; }

        public DbSet<Trademark> Trademark { get; set; }

        public DbSet<Posts> Posts { get; set; }

    }
}
