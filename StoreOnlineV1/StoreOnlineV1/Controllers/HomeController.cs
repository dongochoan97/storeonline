﻿
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StoreOnlineV1.Data;
using StoreOnlineV1.Models;

namespace StoreOnlineV1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly StoreContext _context;
        public const string CARTKEY = "cart";
        public const string USERKEY = "userinfo";

        public HomeController(ILogger<HomeController> logger, StoreContext context)
        {
            _logger = logger;
            _context = context;
        }
        List<CartItem> GetCartItems()
        {

            var session = HttpContext.Session;
            string jsoncart = session.GetString(CARTKEY);
            if (jsoncart != null)
            {
                return JsonConvert.DeserializeObject<List<CartItem>>(jsoncart);
            }
            return new List<CartItem>();
        }
        public async Task<IActionResult> Index()
        {
            var listCart = GetCartItems();
            ViewData["cart"] = listCart.Count;
            // get thong tin user
            var session = HttpContext.Session;
            string jsonUser = session.GetString(USERKEY);
            if (jsonUser != null)
            {
                var userinfo = JsonConvert.DeserializeObject<User>(jsonUser);
                ViewData["UserName"] = userinfo.UserName;
            }
            //
            var allProducts = await _context.Product.OrderByDescending(x =>x.CreateDate).Take(10).ToListAsync();
            var sanphamKhuyenMai = await _context.Product.Where(item => item.Discount > 0).ToListAsync();
            var allPost = await _context.Posts.OrderByDescending(x => x.CreateDate).Take(10).ToListAsync();
            var allTrademark = await _context.Trademark.OrderByDescending(x => x.CreateDate).Take(10).ToListAsync();

            if(allTrademark.Count > 0)
            {

                var idTrademarkFirst = allTrademark[0].Id;
                var allProductByTrademark = await _context.Product.Where(item => item.TrademarkID == idTrademarkFirst).ToListAsync();
                ViewBag.allProductByTrademark = allProductByTrademark;
            }

            ViewBag.ListProducts = sanphamKhuyenMai;
            ViewBag.AllProducts = allProducts;
            ViewBag.allPost = allPost;
            ViewBag.allTrademark = allTrademark;
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
