﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StoreOnlineV1.Data;
using StoreOnlineV1.Models;

namespace StoreOnlineV1.Controllers
{
    public class DetailProductsController : Controller
    {
        private readonly ILogger<DetailProductsController> _logger;
        private readonly StoreContext _context;
        // Key lưu chuỗi json của Cart
        public const string CARTKEY = "cart";
        public const string USERKEY = "userinfo";
        public DetailProductsController(ILogger<DetailProductsController> logger, StoreContext context)
        {
            _logger = logger;
            _context = context;
        }

        // Lấy cart từ Session (danh sách CartItem)
        List<CartItem> GetCartItems()
        {

            var session = HttpContext.Session;
            string jsoncart = session.GetString(CARTKEY);
            if (jsoncart != null)
            {
                return JsonConvert.DeserializeObject<List<CartItem>>(jsoncart);
            }
            return new List<CartItem>();
        }

        // Xóa cart khỏi session
        void ClearCart()
        {
            var session = HttpContext.Session;
            session.Remove(CARTKEY);
        }

        // Lưu Cart (Danh sách CartItem) vào session
        void SaveCartSession(List<CartItem> ls)
        {
            var session = HttpContext.Session;
            string jsoncart = JsonConvert.SerializeObject(ls);
            session.SetString(CARTKEY, jsoncart);
        }

        public async Task<IActionResult> IndexAsync(long? idProduct)
        {
            var listCart = GetCartItems();
            ViewData["cart"] = listCart.Count;
            // get thong tin user
            var session = HttpContext.Session;
            string jsonUser = session.GetString(USERKEY);
            if (jsonUser != null)
            {
                var userinfo = JsonConvert.DeserializeObject<User>(jsonUser);
                ViewData["UserName"] = userinfo.UserName;
            }
            //
            var product = await _context.Product.FirstOrDefaultAsync(m => m.Id == idProduct);
            if (product == null)
            {
                return NotFound();
            }
            ViewBag.Products = product;
            //
            var productCategory = await _context.ProductCategory.FirstOrDefaultAsync(m => m.Id == product.CategoryID);
            var categoryID = productCategory.ParentID;
            var allProductCategory = await _context.ProductCategory.Where(ProductCategory => ProductCategory.ParentID == categoryID).ToListAsync();
            var allProductsByCategory = new List<Product> { };
            allProductCategory.ForEach(async ProductCategory =>
            {
                var allProducts = await _context.Product.Where(product => product.CategoryID == ProductCategory.Id).ToListAsync();
                allProducts.ForEach(itemProduct =>
                {
                    allProductsByCategory.Add(itemProduct);
                });

            });
            ViewBag.AllProductsWithCategory = allProductsByCategory;
            return View();
        }

        /// Thêm sản phẩm vào cart
        public async Task<IActionResult> AddToCartCurrent(long? productid)
        {

            var product = await _context.Product.FirstOrDefaultAsync(m => m.Id == productid);

            if (product == null)
                return NotFound("Không có sản phẩm");

            // Xử lý đưa vào Cart ...
            var cart = GetCartItems();
            var cartitem = cart.Find(p => p.ProductId == productid);
            if (cartitem != null)
            {
                // Đã tồn tại, tăng thêm 1
                cartitem.Quantity++;
            }
            else
            {
                //  Thêm mới
                cart.Add(new CartItem() { Quantity = 1, ProductId = (long)productid });

            }
            // Lưu cart vào Session
            SaveCartSession(cart);
            var listCart = GetCartItems();
            ViewData["cart"] = listCart.Count;
            return RedirectToAction("Index", "Products", new { id = -1 });
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
