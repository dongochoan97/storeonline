﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StoreOnlineV1.Data;
using StoreOnlineV1.Models;

namespace StoreOnlineV1.Controllers
{
    public class ContactController : Controller
    {
        private readonly ILogger<ContactController> _logger;
        private readonly StoreContext _context;

        // Key lưu chuỗi json của Cart
        public const string CARTKEY = "cart";
        public const string USERKEY = "userinfo";

        // Lấy cart từ Session (danh sách CartItem)
        List<CartItem> GetCartItems()
        {

            var session = HttpContext.Session;
            string jsoncart = session.GetString(CARTKEY);
            if (jsoncart != null)
            {
                return JsonConvert.DeserializeObject<List<CartItem>>(jsoncart);
            }
            return new List<CartItem>();
        }
        public ContactController(ILogger<ContactController> logger, StoreContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<IActionResult> IndexAsync()
        {
            var cart = GetCartItems();
            ViewData["cart"] = cart.Count;
            // get thong tin user
            var session = HttpContext.Session;
            string jsonUser = session.GetString(USERKEY);
            if (jsonUser != null)
            {
                var userinfo = JsonConvert.DeserializeObject<User>(jsonUser);
                ViewData["UserName"] = userinfo.UserName;
                ViewBag.UserInfo = userinfo;
                var allAbout = await _context.Introduce.Take(10).ToListAsync();
                if (allAbout.First() != null)
                {
                    ViewBag.Introduce = allAbout.First();
                }
            }
            else
            {
                return RedirectToAction("Login", "Acount");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Feedback(Feedback feedback)
        {

            if (ModelState.IsValid)
            {
                var session = HttpContext.Session;
                string jsonUser = session.GetString(USERKEY);
                if (jsonUser != null)
                {
                    var userinfo = JsonConvert.DeserializeObject<User>(jsonUser);
                    //
                    var acount =  await _context.User.FirstOrDefaultAsync(m => m.Id == userinfo.Id);
                    if(acount.Password == userinfo.Password)
                    {
                        feedback.CreateDate = DateTime.Now;
                        feedback.Status = false;
                        _context.Add(feedback);
                        await _context.SaveChangesAsync();
                        return RedirectToAction("Home", "Index");
                    }
                }
                
            }
            // Chuyển đến trang hiện thị Cart
            return RedirectToAction(nameof(Index));
        }

    }
}
