﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StoreOnlineV1.Data;
using StoreOnlineV1.Models;

namespace StoreOnlineV1.Controllers
{
    public class ProductsController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly StoreContext _context;
        public const string CARTKEY = "cart";
        public const string USERKEY = "userinfo";
        public ProductsController(ILogger<HomeController> logger, StoreContext context)
        {
            _logger = logger;
            _context = context;
        }

       public async Task<IActionResult> Index(int? idCategory, string sortby, string query)
        {
            
            var listCart = GetCartItems();
            ViewData["cart"] = listCart.Count;
            // get thong tin user
            var session = HttpContext.Session;
            string jsonUser = session.GetString(USERKEY);
            if (jsonUser != null)
            {
                var userinfo = JsonConvert.DeserializeObject<User>(jsonUser);
                ViewData["UserName"] = userinfo.UserName;
            }
            //
            var allCategory = await _context.Categories.ToListAsync();
            ViewBag.allCategory = allCategory;
            if (idCategory == -1 || idCategory == null)
            {
                ViewData["CategoryName"] = "Tất cả sản phẩm";
                var allProductHost = await _context.Product.OrderByDescending(x => x.CreateDate).ToListAsync();
                ViewBag.allProductHost = allProductHost;
                if (query != null)
                {
                    var allProducts = new List<Product>();
                    if (sortby == null) sortby = "alpha-desc";
                    allProducts = await _context.Product.Where(ProductCategory => ProductCategory.Name.Contains(query)).OrderByDescending(s => s.Name).ToListAsync();
                    ViewBag.AllProducts = allProducts;
                }
                else
                {
                    var allProducts = new List<Product>();
                    if (sortby == null) sortby = "alpha-desc";
                    switch (sortby)
                    {
                        case "alpha-desc":
                            allProducts = await _context.Product.OrderByDescending(s => s.Name).ToListAsync();
                            break;
                        case "created-desc":
                            allProducts = await _context.Product.OrderByDescending(s => s.CreateDate).ToListAsync();
                            break;
                        case "created-asc":
                            allProducts = await _context.Product.OrderBy(s => s.CreateDate).ToListAsync();
                            break;
                        case "alpha-asc":
                            allProducts = await _context.Product.OrderBy(s => s.Name).ToListAsync();
                            break;
                        case "price-desc":
                            allProducts = await _context.Product.OrderByDescending(s => (double?)s.PromotionPrice).ToListAsync();
                            break;
                        case "price-asc":
                            allProducts = await _context.Product.OrderBy(s => (double?)s.PromotionPrice).ToListAsync();
                            break;
                    }
                    ViewBag.AllProducts = allProducts;

                }


            }
            else
            {
                var allProductCategory = await _context.ProductCategory.Where(ProductCategory => ProductCategory.ParentID == idCategory).ToListAsync();
                var category = await _context.Categories.FirstOrDefaultAsync(m => m.Id == idCategory);
                ViewData["CategoryName"] = category.Name;
                var allProductsByCategory = new List<Product> { };
                allProductCategory.ForEach(async ProductCategory =>
                {
                    var allProducts = await _context.Product.Where(product => product.CategoryID == ProductCategory.Id).ToListAsync();
                    allProducts.ForEach(itemProduct =>
                    {
                        allProductsByCategory.Add(itemProduct);
                    });

                });
                if (sortby == null) sortby = "alpha-desc";
                switch (sortby)
                {
                    case "alpha-desc":
                        allProductsByCategory = allProductsByCategory.OrderByDescending(s => s.Name).ToList();
                        break;
                    case "created-desc":
                        allProductsByCategory = allProductsByCategory.OrderByDescending(s => s.CreateDate).ToList();
                        break;
                    case "created-asc":
                        allProductsByCategory = allProductsByCategory.OrderBy(s => s.CreateDate).ToList();
                        break;
                    case "alpha-asc":
                        allProductsByCategory = allProductsByCategory.OrderBy(s => s.Name).ToList();
                        break;
                    case "price-desc":
                        allProductsByCategory = allProductsByCategory.OrderByDescending(s => (double?)s.PromotionPrice).ToList();
                        break;
                    case "price-asc":
                        allProductsByCategory = allProductsByCategory.OrderBy(s => (double?)s.PromotionPrice).ToList();
                        break;
                }

                ViewBag.AllProducts = allProductsByCategory;

                var allProductHost = await _context.Product.OrderByDescending(x => x.CreateDate).Take(5).ToListAsync();
                ViewBag.allProductHost = allProductHost;
            }
            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        List<CartItem> GetCartItems()
        {

            var session = HttpContext.Session;
            string jsoncart = session.GetString(CARTKEY);
            if (jsoncart != null)
            {
                return JsonConvert.DeserializeObject<List<CartItem>>(jsoncart);
            }
            return new List<CartItem>();
        }
    }
}
