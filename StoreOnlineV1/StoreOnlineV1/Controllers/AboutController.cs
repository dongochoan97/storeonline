﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StoreOnlineV1.Data;
using StoreOnlineV1.Models;

namespace StoreOnlineV1.Controllers
{
    public class AboutController : Controller
    {
        private readonly ILogger<AboutController> _logger;
        public const string USERKEY = "userinfo";
        public const string CARTKEY = "cart";
        private readonly StoreContext _context;
        public AboutController(ILogger<AboutController> logger, StoreContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async System.Threading.Tasks.Task<IActionResult> IndexAsync(int? id)
        {
            // get thong tin user
            var session = HttpContext.Session;
            string jsonUser = session.GetString(USERKEY);
            if (jsonUser != null)
            {
                var userinfo = JsonConvert.DeserializeObject<User>(jsonUser);
                ViewData["UserName"] = userinfo.UserName;
            }
            var listCart = GetCartItems();
            ViewData["cart"] = listCart.Count;
            //
            var allAbout = await _context.Introduce.Take(10).ToListAsync();
            if (allAbout.First() != null)
            {
                ViewData["content_about"] = allAbout.First().Content;
            }
            //
            var allCategory = await _context.Categories.ToListAsync();
            ViewBag.allCategory = allCategory;
            return View();
        }
        // Lấy cart từ Session (danh sách CartItem)
        List<CartItem> GetCartItems()
        {

            var session = HttpContext.Session;
            string jsoncart = session.GetString(CARTKEY);
            if (jsoncart != null)
            {
                return JsonConvert.DeserializeObject<List<CartItem>>(jsoncart);
            }
            return new List<CartItem>();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
