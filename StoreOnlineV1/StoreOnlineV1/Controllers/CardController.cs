﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StoreOnlineV1.Data;
using StoreOnlineV1.Models;

namespace StoreOnlineV1.Controllers
{
    public class CardController : Controller
    {
        private readonly ILogger<CardController> _logger;
        private readonly StoreContext _context;

        // Key lưu chuỗi json của Cart
        public const string CARTKEY = "cart";
        public const string USERKEY = "userinfo";
        public CardController(ILogger<CardController> logger, StoreContext context)
        {
            _logger = logger;
            _context = context;
        }
        // Lấy cart từ Session (danh sách CartItem)
        List<CartItem> GetCartItems()
        {

            var session = HttpContext.Session;
            string jsoncart = session.GetString(CARTKEY);
            if (jsoncart != null)
            {
                return JsonConvert.DeserializeObject<List<CartItem>>(jsoncart);
            }
            return new List<CartItem>();
        }

        // Xóa cart khỏi session
        void ClearCart()
        {
            var session = HttpContext.Session;
            session.Remove(CARTKEY);
        }

        // Lưu Cart (Danh sách CartItem) vào session
        void SaveCartSession(List<CartItem> ls)
        {
            var session = HttpContext.Session;
            string jsoncart = JsonConvert.SerializeObject(ls);
            session.SetString(CARTKEY, jsoncart);
        }
        public IActionResult Index()
        {
            var cart = GetCartItems();
            var allCart = new List<CartViewModel>();
            cart.ForEach(async (CartItem obj) =>
           {
               var product = await _context.Product.FirstOrDefaultAsync(m => m.Id == obj.ProductId);
               var cardInfo = new CartViewModel();
               cardInfo.Id = product.Id;
               cardInfo.Name = product.Name;
               cardInfo.Code = product.Code;
               cardInfo.Image = product.Image;
               cardInfo.Price = (decimal)(product.PromotionPrice - (product.PromotionPrice * product.Discount / 100));
               cardInfo.Quantity = obj.Quantity;
               allCart.Add(cardInfo);
           });


            ViewData["cart"] = cart.Count;
            ViewBag.allCart = allCart;
            // get thong tin user
            var session = HttpContext.Session;
            string jsonUser = session.GetString(USERKEY);
            if (jsonUser != null)
            {
                var userinfo = JsonConvert.DeserializeObject<User>(jsonUser);
                ViewData["UserName"] = userinfo.UserName;
            }
            //
            return View();
        }

        /// Thêm sản phẩm vào cart
        public async Task<IActionResult> AddToCart(long? productid)
        {

            var product = await _context.Product.FirstOrDefaultAsync(m => m.Id == productid);

            if (product == null)
                return NotFound("Không có sản phẩm");

            // Xử lý đưa vào Cart ...
            var cart = GetCartItems();
            var cartitem = cart.Find(p => p.ProductId == productid);
            if (cartitem != null)
            {
                // Đã tồn tại, tăng thêm 1
                cartitem.Quantity++;
            }
            else
            {
                //  Thêm mới
                cart.Add(new CartItem() { Quantity = 1, ProductId = (long)productid });

            }

            // Lưu cart vào Session
            SaveCartSession(cart);
            // Chuyển đến trang hiện thị Cart
            return RedirectToAction(nameof(Index));
            //return View();
        }

        /// Thêm sản phẩm vào cart
        [HttpPost]
        public async Task<IActionResult> AddToCart(CartItem cartItem)
        {

            var product = await _context.Product.FirstOrDefaultAsync(m => m.Id == cartItem.ProductId);

            if (product == null)
                return NotFound("Không có sản phẩm");

            // Xử lý đưa vào Cart ...
            var cart = GetCartItems();
            var cartitem = cart.Find(p => p.ProductId == cartItem.ProductId);
            if (cartitem != null)
            {
                // Đã tồn tại, tăng thêm 1
                cartitem.Quantity += cartItem.Quantity;
            }
            else
            {
                //  Thêm mới
                cart.Add(new CartItem() { Quantity = cartItem.Quantity, ProductId = cartItem.ProductId });

            }

            // Lưu cart vào Session
            SaveCartSession(cart);
            // Chuyển đến trang hiện thị Cart
            return RedirectToAction(nameof(Index));
        }

        /// Thêm sản phẩm vào cart
        [HttpPost]
        public async Task<IActionResult> UpdateToCart(List<CartItem> data)
        {
            ClearCart();
            var cart = new List<CartItem>();
            for (var i = 0; i < data.Count; i++)
            {
             var product = await _context.Product.FirstOrDefaultAsync(m => m.Id == data[i].ProductId);

            if (product == null)
                return NotFound("Không có sản phẩm");

            cart.Add(new CartItem() { Quantity = data[i].Quantity, ProductId = data[i].ProductId });

            }
            //Lưu cart vào Session
            SaveCartSession(cart);
            // Chuyển đến trang hiện thị Cart
            return RedirectToAction(nameof(Index));
        }


        public IActionResult DeleteCart(long? id)
        {

            var cart = GetCartItems();
            var cartitem = cart.Find(p => p.ProductId == id);
            if (cartitem != null)
            {
                // Đã tồn tại, tăng thêm 1
                cart.Remove(cartitem);
            }
            // Lưu cart vào Session
            SaveCartSession(cart);
            // Chuyển đến trang hiện thị Cart
            return RedirectToAction(nameof(Index));
            //return View();
        }

        public async Task<IActionResult> PaymentAsync(PayViewModel dataPay)
        {

            var cart = GetCartItems();
            var session = HttpContext.Session;
            string jsonUser = session.GetString(USERKEY);
            if (jsonUser != null)
            {
                var userinfo = JsonConvert.DeserializeObject<User>(jsonUser);
                var _user = await _context.User.FirstOrDefaultAsync(m => m.UserName == userinfo.UserName);
                if (_user != null)
                {
                    if (userinfo.Password == _user.Password)
                    {
                        var oderInfo = new Order();
                        decimal totalMoney = 0;
                        oderInfo.ID_User = userinfo.Id;
                        oderInfo.CreateDate = DateTime.Now;
                        oderInfo.Status = false;
                        oderInfo.Note = "Người nhận: " +dataPay.Name + " - Số điện thoai: " +dataPay.Phone + "- Địa chỉ nhận: "+dataPay.Address +"- Note : "+dataPay.Note;
                        _context.Add(oderInfo);
                        await _context.SaveChangesAsync();
                        for (var i = 0; i< cart.Count; i++)
                        {
                            var product = await _context.Product.FirstOrDefaultAsync(m => m.Id == cart[i].ProductId);
                            var oderDetail = new OrderDetail { ID_Order = (long)oderInfo.Id,
                                ID_Product = cart[i].ProductId,
                                Quantity = cart[i].Quantity,
                                PriceImport = product.Price,
                                Price = product.PromotionPrice,
                                Sale = product.Discount
                            };
                            product.Quantity = product.Quantity - cart[i].Quantity;
                            _context.Update(product);
                            var _price = (product.PromotionPrice - (product.PromotionPrice * decimal.Divide((decimal)product.Discount, 100))) * cart[i].Quantity;
                            totalMoney += _price ?? 0;
                            _context.Add(oderDetail);
                        }
                        await _context.SaveChangesAsync();
                        var oderUpdate = await _context.Order.FirstOrDefaultAsync(m => m.Id == oderInfo.Id);
                        oderUpdate.Money = totalMoney;
                        _context.Update(oderUpdate);
                        await _context.SaveChangesAsync();
                        ClearCart();

                    }
                    else
                    {
                        return RedirectToAction("Login", "Acount");
                    }
                }
            }
            // Chuyển đến trang hiện thị Cart
            return RedirectToAction(nameof(Index));
            //return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
