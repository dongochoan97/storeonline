﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StoreOnlineV1.Common;
using StoreOnlineV1.Data;
using StoreOnlineV1.Models;

namespace StoreOnlineV1.Controllers
{
    public class AcountController : Controller
    {
        private readonly ILogger<AboutController> _logger;
        private readonly StoreContext _context;
        // Key lưu chuỗi json của Cart
        public const string USERKEY = "userinfo";
        public const string CARTKEY = "cart";
        public AcountController(ILogger<AboutController> logger, StoreContext context)
        {
            this._logger = logger;
            this._context = context;
        }
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(User user)
        {
            var _user = await _context.User
               .FirstOrDefaultAsync(m => m.UserName == user.UserName);
            if (_user != null)
            {
                if(Encryptor.MD5Hash(user.Password) == _user.Password)
                {
                    var session = HttpContext.Session;
                    string jsoncart = JsonConvert.SerializeObject(_user);
                    session.SetString(USERKEY, jsoncart);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewData["message"] = "Đăng nhập thất bại !";
                }
            }
            return View();
        }
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register( User user)
        {

            if (ModelState.IsValid)
            {
                if (user.Password == "" || user.Password == null) user.Password = "12345678"; // mat khau mac dinh neu user khong nhap
                user.CreateDate = DateTime.Now;
                user.ModifiedDate = DateTime.Now;
                user.CreateBy = "Admin";
                user.ModifiedBy = "Admin";
                user.Status = false;
                var encryterdMd5 = Encryptor.MD5Hash(user.Password);
                user.Password = encryterdMd5;
                _context.Add(user);
                var result  = await _context.SaveChangesAsync();
                if(result == 1) { 
                    var session = HttpContext.Session;
                    string jsoncart = JsonConvert.SerializeObject(user);
                    session.SetString(USERKEY, jsoncart);
                }
                return RedirectToAction("Index","Home");
            }

            return View();
        }

        public IActionResult Logout()
        {
            var session = HttpContext.Session;
            session.Remove(USERKEY);
            session.Remove(CARTKEY);
            return RedirectToAction("Index", "Home");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
